module.exports = function () {
    var faker = require('faker')
    var _ = require('lodash')

    return {
        todos: _.times(_.random(7, 10), function (n) {
            return {
                id: n,
                name: faker.lorem.sentence(_.random(3, 12)),
                completed: faker.random.boolean()
            }
        })
    }
}
