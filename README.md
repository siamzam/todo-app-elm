## Todo App

### Instructions
Install dependencies

``` bash
npm install
```

The REST Api is mocked with json-server. The server runs on localhost port 3000.

To start the server with an empty todo list run:

``` bash
json-server --watch empty.json
```

To start the server with mocked todos run:
``` bash
json-server mock.js
```

To build the project run the following command from another terminal:
``` bash
npm run dev
```

Open http://localhost:8090 in a browser of your choice to view the app.
