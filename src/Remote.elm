module Remote exposing (..)

import Http
import Json.Decode as Decode
import Json.Encode as Encode


import Msg exposing (Msg(..))
import Model.Todo as Todo exposing (Todo)


fetchTodos : Cmd Msg
fetchTodos =
    Http.get
        { url = baseUrl ++ "/todos"
        , expect = Http.expectJson GotTodos (Todo.todosDecoder)
        }


addNewTodo : String -> Cmd Msg
addNewTodo name =
    Http.post
        { url = baseUrl ++ "/todos"
        , body =
            Encode.object
                [ ("name", Encode.string name)
                , ("completed", Encode.bool False)
                ]
                    |> Http.jsonBody
        , expect = Http.expectJson GotNewTodo (Todo.todoDecoder)
        }


syncTodo : Todo -> Cmd Msg
syncTodo todo =
    Http.request
        { method = "PUT"
        , headers = []
        , url = baseUrl ++ "/todos/" ++ String.fromInt todo.id
        , body = Http.jsonBody <| Todo.todoEncoder todo
        , expect = Http.expectJson GotTodo (Todo.todoDecoder)
        , timeout = Nothing
        , tracker = Nothing
        }


removeTodo : Int -> Cmd Msg
removeTodo id =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = baseUrl ++ "/todos/" ++ String.fromInt id
        , body = Http.emptyBody
        , expect = Http.expectJson RemovedTodo (Decode.succeed id)
        , timeout = Nothing
        , tracker = Nothing
        }



baseUrl : String
baseUrl =
    "http://localhost:3000"
