module View.Icons.CheckMark exposing (render)

import Element exposing (Element, html)
import Svg exposing (..)
import Svg.Attributes as Attributes exposing (..)


render : String -> Element msg
render color =
    let
        styles =
            [ ( "fill", color )
            , ( "width", "100%" )
            , ( "height", "100%" )
            , ( "-webkit-transform", "rotate(225deg) translate(12%, 12%)" )
            , ( "transform", "rotate(225deg) translate(12%, 12%)" )
            ]
    in
        svg
            [ version "1.1"
            , viewBox "0 0 54 54"
            , Attributes.style <| joinStyles styles
            ]
            [ g
                [ fill "none"
                , fillRule "evenodd"
                , stroke "none"
                , strokeWidth "1"
                ]
                [ g
                    [ stroke color
                    ]
                    [ node "rect"
                        [ height "1"
                        , width "15"
                        , x "15"
                        , y "15"
                        ]
                        []
                    , node "rect"
                        [ height "30"
                        , width "1"
                        , x "15"
                        , y "15"
                        ]
                        []
                    ]
                ]
            ]
                |> html


joinStyles : List ( String, String ) -> String
joinStyles styles =
    let
        join ( x, y ) =
            x ++ ":" ++ y
    in
        List.map join styles |> String.join ";"
