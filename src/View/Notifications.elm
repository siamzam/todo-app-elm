module View.Notifications exposing (render)


import Element exposing (..)
import Element.Font as Font
import Element.Background as Bg
import Element.Border as Border
import Html.Attributes exposing (style)
import Animation

import State exposing (State)
import Model.Notification exposing (Notification, Kind(..))
import View.CommonStyles exposing (colors)


render : State -> Element msg
render state =
    state.notifications
        |> List.map renderNotification
        |> column
            (
                [ spacing 20
                , width <| maximum 800 fill
                , padding 20
                , centerX
                ]
                    ++ positionFixed
            )


renderNotification : Notification -> Element msg
renderNotification notification =
    el
        (
            [ width fill
            , padding 20
            , Border.rounded 5
            , Font.color <| .white <| colors 1
            ]
                ++ styles notification.kind
                ++
                    ( Animation.render notification.animation
                        |> List.map htmlAttribute
                    )

        )
        <| text notification.message


styles : Kind -> List (Attribute msg)
styles kind =
    case kind of
        Success ->
            [ Bg.color <| .success <| colors 1
            , boxShadow <| .success <| colors 0.5
            ]

        Error ->
            [ Bg.color <| .error <| colors 1
            , boxShadow <| .error <| colors 0.5
            ]


boxShadow : Color -> Attr decorative msg
boxShadow color =
    Border.shadow
        { offset = (0, 2)
        , size = 0
        , blur = 8
        , color = color
        }


positionFixed : List (Attribute msg)
positionFixed =
    [ ("position", "fixed")
    , ("bottom", "0")

    ]
        |> List.map
            (\(prop, val) ->
                style prop val
                    |> htmlAttribute
            )
