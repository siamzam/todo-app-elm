module View.Todos exposing (render)

import Dict
import Html.Events
import Html.Attributes exposing (style)
import Element exposing (..)
import Element.Input as Input
import Element.Font as Font
import Element.Border as Border
import Element.Background as Bg

import Msg exposing (Msg(..))
import State exposing (State)
import Model.Todo exposing (Todo)
import View.Components.Input as Input
import View.Components.Checkbox as Checkbox
import View.Components.Button as Button
import View.CommonStyles exposing (colors)


render : State -> Element Msg
render state =
    case Dict.isEmpty state.todos of
        True ->
            el
                [ Font.color <| .primary <| colors 1
                , Font.center
                , width fill
                ]
                <| text "The todo list is empty. Use the input above to Rock & Roll!"

        False ->
            Dict.toList state.todos
                |> List.map Tuple.second
                |> List.reverse
                |> List.map (renderTodo state)
                |> column
                    [ width fill
                    , spacing 20
                    ]


renderTodo : State -> Todo -> Element Msg
renderTodo state todo =
    row
        (
            [ spacing 20
            , width fill
            ]
        )
        [ renderInput todo
        , renderCompleted todo
        , renderButton todo.id
        ]


renderInput : Todo -> Element Msg
renderInput todo =
    Input.render
        { onChange = Msg.updateTodoName todo
        , onEnter = Nothing
        , onFocusOut = Just (Msg.syncTodo todo)
        , label = Input.labelHidden "Todo Name"
        , value = todo.name
        , style =
            case todo.completed of
                True ->
                    [ Bg.color <| .primary <| colors 1
                    , Font.color <| .white <| colors 1
                    ]

                False ->
                    [style "background-color" "transparent" |> htmlAttribute ]
        }


renderCompleted : Todo -> Element Msg
renderCompleted todo =
    Checkbox.render
        { onChange = Msg.updateTodoStatus todo
        , label = Input.labelHidden "Todo Status"
        , value = todo.completed
        , style =
            if todo.completed then
                [ Bg.color <| .primary <| colors 1 ]
            else
                []
        }


renderButton : Int -> Element Msg
renderButton id =
    Button.render
        { onPress = Msg.removeTodo id
        , label = el [] <| text "Remove"
        , style = []
        }
