module View.Stats exposing (render)

import Dict
import Html.Attributes exposing (style)
import Element exposing (..)
import Element.Border as Border
import Element.Background as Bg

import State exposing (State)
import Model.Todo exposing (Todos)
import View.CommonStyles exposing (boxShadow, colors)

render : State -> Element msg
render state =
    let
        completed =
            getCompletedPercentage state.todos

        bgColor =
            if completed == 100 then
                .primary <| colors 1
            else
                .primaryLight <| colors 1
    in
    if Dict.isEmpty state.todos then
        none
    else
        el
            [ style "width" (String.fromFloat completed ++ "%")
                |> htmlAttribute
            , height <| px 15
            , Bg.color bgColor
            , Border.rounded 15
            , style "transition" "all 300ms ease" |> htmlAttribute
            ]
            none
                |> el
                    [ Border.rounded 15
                    , padding 5
                    , width fill
                    , boxShadow <| .black <| colors 0.1
                    ]


getCompletedPercentage : Todos -> Float
getCompletedPercentage todos =
    todos
        |> Dict.toList
        |> List.foldr
            (\(id, todo) count ->
                case todo.completed of
                    True ->
                        count + 1

                    False ->
                        count
            )
            0
        |> (*) (100 / toFloat (Dict.size todos))
