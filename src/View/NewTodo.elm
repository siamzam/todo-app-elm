module View.NewTodo exposing (render)


import Element exposing (..)
import Element.Input as Input

import Msg exposing (Msg)
import State exposing (State)
import View.Components.Input as Input
import View.Components.Button as Button


render : State -> Element Msg
render state =
    row
        [ spacing 20
        , width fill
        ]
        [ renderNameInput state
        , renderButton
        ]


renderNameInput : State -> Element Msg
renderNameInput state =
    Input.render
        { onChange = Msg.UpdateNewTodoName
        , onEnter = Just Msg.AddNewTodo
        , onFocusOut = Nothing
        , label = Input.labelHidden "New Todo"
        , value = Maybe.withDefault "" state.newTodoName
        , style = []
        }


renderButton : Element Msg
renderButton =
    Button.render
        { onPress = Just Msg.AddNewTodo
        , label = el [] <| text "Add"
        , style = []
        }
