module View.Heading exposing (render)

import Element exposing (..)
import Element.Font as Font

import View.CommonStyles exposing (colors)


render : Element msg
render =
    el
        [ Font.size 26
        , Font.bold
        , Font.center
        , Font.color <| .primary <| colors 1
        , width fill
        ]
        <| text "TODO APP (Elm)"
