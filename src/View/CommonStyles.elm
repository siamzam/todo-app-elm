module View.CommonStyles exposing (..)


import Html.Attributes exposing (style)
import Element exposing (..)
import Element.Border as Border


commonStyles : List (Attribute msg)
commonStyles =
    [ boxShadow <| .black <| colors 0.1
    , mouseOver
        [ boxShadow <| .primaryLight <| colors 0.5
        , moveUp 2
        ]
    , focused
        [ boxShadow <| .primaryLight <| colors 0.5
        , moveUp 2
        ]
    , style "transition" "all 300ms ease" |> htmlAttribute
    ]


boxShadow : Color -> Attr decorative msg
boxShadow color =
    Border.shadow
        { offset = (0, 2)
        , size = 0
        , blur = 8
        , color = color
        }


type alias Colors =
    { primary : Color
    , primaryLight : Color
    , black : Color
    , white : Color
    , bg : Color
    , error : Color
    , success : Color
    }


colors : Float -> Colors
colors opacity =
    { primary = rgba255 153 10 227 opacity
    , primaryLight = rgba255 192 0 240 opacity
    , black = rgba255 2 2 2 opacity
    , white = rgba255 255 255 255 opacity
    , bg = rgba255 242 242 242 opacity
    , error = rgba255 204 0 0 opacity
    , success = rgba255 75 181 67 opacity
    }
