module View.Components.Button exposing (render)

import Element exposing (..)
import Element.Input as Input
import Element.Border as Border
import View.CommonStyles exposing (commonStyles)


type alias Props msg =
    { onPress : Maybe msg
    , label : Element msg
    , style : List (Attribute msg)
    }


render : Props msg -> Element msg
render props =
    Input.button
        (
            [ Border.rounded 5
            , paddingXY 10 15
            ]
                ++ commonStyles
                ++ props.style
        )
        { onPress = props.onPress
        , label = props.label
        }
