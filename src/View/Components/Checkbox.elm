module View.Components.Checkbox exposing (render)

import Html
import Html.Events
import Element exposing (..)
import Element.Input as Input
import Element.Border as Border
import Element.Background as Bg

import View.CommonStyles exposing (commonStyles, colors)
import View.Icons.CheckMark as CheckMark


type alias Props msg =
    { onChange : Bool -> msg
    , label : Input.Label msg
    , value : Bool
    , style : List (Attribute msg)
    }


render : Props msg -> Element msg
render props =
    Input.checkbox
        (
            [ width shrink
            , paddingEach { left = 0, top = 7, right = 3, bottom = 3 }
            , Bg.color <| .primaryLight <| colors 0.5
            ]
                ++ commonStyles
                ++ props.style
        )
        { onChange = props.onChange
        , icon = renderIcon
        , checked = props.value
        , label = props.label
        }


renderIcon : Bool -> Element msg
renderIcon checked =
    el
        [ width (px 32)
        , height (px 25)
        ]
        (
            if checked then
                CheckMark.render "#fff"
            else
                none
        )
