module View.Components.Input exposing (render)

import Html
import Html.Events
import Html.Attributes exposing (style)
import Element exposing (..)
import Element.Input as Input
import Element.Border as Border
import Json.Decode as Decode exposing (Decoder)

import View.CommonStyles exposing (commonStyles)


type alias Props msg =
    { onChange : String -> msg
    , onEnter : Maybe msg
    , onFocusOut : Maybe (Decoder msg)
    , label : Input.Label msg
    , value : String
    , style : List (Attribute msg)
    }


render : Props msg -> Element msg
render props =
    let
        onFocusOut =
            props.onFocusOut
                |> Maybe.map
                    ( Html.Events.on "focusout"
                        >> htmlAttribute
                        >> List.singleton
                    )
                |> Maybe.withDefault []
    in
    Input.text
        (
            [ Border.width 0
            , Border.rounded 5
            , padding 15
            ]
                ++ onFocusOut
                ++ onEnter props.onEnter
                ++ commonStyles
                ++ props.style
        )
        { onChange = props.onChange
        , text = props.value
        , placeholder = Nothing
        , label = props.label
        }


onEnter : Maybe msg -> List (Attribute msg)
onEnter msg =
    case msg of
        Just action ->
            Html.Events.on "keyup"
                ( Decode.map2
                    Tuple.pair Html.Events.targetValue Html.Events.keyCode
                        |> Decode.andThen
                            (\(value, code) ->
                                if code == 13 then
                                    Decode.succeed action
                                else
                                    Decode.fail ""
                            )
                )
                    |> htmlAttribute
                    |> List.singleton

        Nothing ->
            []
