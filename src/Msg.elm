module Msg exposing (..)

import Http
import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Animation

import Model.Todo exposing (Todo)
import Model.Notification exposing (Notification)


type Msg
    = FetchTodos
    | UpdateTodo Todo
    | UpdateNewTodoName String
    | AddNewTodo
    | SyncTodo Todo
    | RemoveTodo Int
    | RemovedTodo (Result Http.Error Int)
    | GotTodo (Result Http.Error Todo)
    | GotNewTodo (Result Http.Error Todo)
    | GotTodos (Result Http.Error (Dict Int Todo))
    | AddNotification Notification
    | Notify
    | RemoveNotification
    | Animate Animation.Msg
    | NoOp


updateTodoName : Todo -> String -> Msg
updateTodoName todo name =
    UpdateTodo { todo | name = name }


updateTodoStatus : Todo -> Bool -> Msg
updateTodoStatus todo completed =
    UpdateTodo { todo | completed = completed }


syncTodo : Todo -> Decoder Msg
syncTodo =
    SyncTodo >> Decode.succeed


removeTodo : Int -> Maybe Msg
removeTodo =
    RemoveTodo >> Just
