module Main exposing (main)

import Url exposing (Url)
import Html exposing (Html)
import Browser
import Remote
import Animation

import Msg exposing (Msg(..))
import State exposing (State)
import Update exposing (update)
import View exposing (view)


init : () -> (State, Cmd Msg)
init _ =
    (State.empty, Remote.fetchTodos)


subscriptions : State -> Sub Msg
subscriptions state =
    state.notifications
        |> List.map (.animation)
        |> Animation.subscription Animate


main : Program () State Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
