module Model.Notification exposing (..)

import Animation


type Kind
    = Error
    | Success


type alias Notification =
    { kind: Kind
    , message : String
    , animation : Animation.State
    }


error : String -> Notification
error message =
    Notification Error message animation

success : String -> Notification
success message =
    Notification Success message animation


animation : Animation.State
animation =
    Animation.style
        [ Animation.bottom (Animation.px -20)
        , Animation.opacity 0
        ]
