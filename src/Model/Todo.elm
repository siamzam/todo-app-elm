module Model.Todo exposing (..)


import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder)
import Json.Encode as Encode exposing (Value)

type alias Todo =
    { id : Int
    , name : String
    , completed : Bool
    }


type alias Todos =
    Dict Int Todo


empty : Todos
empty =
    Dict.empty


todoDecoder : Decoder Todo
todoDecoder =
    Decode.map3 Todo
        (Decode.field "id" Decode.int)
        (Decode.field "name" Decode.string)
        (Decode.field "completed" Decode.bool)


todosDecoder : Decoder Todos
todosDecoder =
    Decode.list todoDecoder
        |> Decode.andThen
            (List.map (\todo -> (todo.id, todo))
                >> Dict.fromList
                >> Decode.succeed
            )


todoEncoder : Todo -> Value
todoEncoder todo =
    Encode.object
        [ ("id", Encode.int todo.id)
        , ("name", Encode.string todo.name)
        , ("completed", Encode.bool todo.completed)
        ]
