module State exposing (..)

import Animation exposing (px)
import Animation

import Msg exposing (Msg)
import Model.Todo as Todo exposing (Todo, Todos)
import Model.Notification as Notification exposing (Notification, Kind(..))


type alias State =
    { newTodoName : Maybe String
    , todos : Todos
    , notifications : List Notification
    }


empty : State
empty =
    { newTodoName = Nothing
    , todos = Todo.empty
    , notifications = []
    }
