module Update exposing (update)

import Http
import Dict
import Task
import Process
import Animation
import Time

import Msg exposing (Msg(..))
import State exposing (State)
import Model.Todo as Todo exposing (Todo, Todos)
import Model.Notification as Notification exposing (Notification)
import Remote


--update : Action -> State -> (State, Cmd msg)
update action state =
    case action of
        FetchTodos ->
            ( state
            , Remote.fetchTodos
            )

        GotTodos result ->
            case result of
                Ok todos ->
                    ( todos
                        |> asTodosIn state
                    , case Dict.isEmpty todos of
                        True ->
                            Cmd.none

                        False ->
                            Notification.success "Loaded all the Todos!"
                                |> AddNotification
                                |> toCmd
                    )

                Err _ ->
                    ( state
                    , Notification.error "An error occured while fetching the todos!"
                        |> AddNotification
                        |> toCmd
                    )

        UpdateTodo todo ->
            ( state.todos
                |> Dict.insert todo.id todo
                |> asTodosIn state

            -- Todo: Debounce this message sent from the input
            , SyncTodo todo
                |> toCmd
            )

        UpdateNewTodoName name ->
            ( { state | newTodoName = Just name }
            , Cmd.none
            )

        AddNewTodo ->
            if Dict.size state.todos >= 10 then
                ( state
                , Notification.error "The list is full!"
                    |> AddNotification
                    |> toCmd
                )
            else
                ( state
                , state.newTodoName
                    |> Maybe.map (String.trim >> Remote.addNewTodo)
                    |> Maybe.withDefault
                        ( Notification.error "Todo name cannot be empty!"
                            |> AddNotification
                            |> toCmd
                        )
                )

        SyncTodo todo ->
            ( state
            , Remote.syncTodo todo
            )

        RemoveTodo id ->
            ( state
            , Remote.removeTodo id
            )

        RemovedTodo result ->
            case result of
                Ok id ->
                    ( state.todos
                        |> Dict.remove id
                        |> asTodosIn state
                    , Notification.success "Deleted the todo!"
                        |> AddNotification
                        |> toCmd
                    )

                Err error ->
                    ( state
                    , Notification.error "An error occured while deleting the todo!"
                        |> AddNotification
                        |> toCmd
                    )

        GotTodo result ->
            case result of
                Ok todo ->
                    ( state.todos
                        |> Dict.insert todo.id todo
                        |> asTodosIn state
                    , Notification.success "Updated the todo!"
                        |> AddNotification
                        |> toCmd
                    )

                Err error ->
                    ( state
                    , Notification.error "An error occured while updating the todo!"
                        |> AddNotification
                        |> toCmd
                    )

        GotNewTodo result ->
            case result of
                Ok todo ->
                    ( state.todos
                        |> Dict.insert todo.id todo
                        |> asTodosIn state
                        |> clearNewTodo
                    , Notification.success "Created the new todo!"
                        |> AddNotification
                        |> toCmd
                    )

                Err error ->
                    ( state
                    , Notification.error "An error occured while creating the todo!"
                        |> AddNotification
                        |> toCmd
                    )

        AddNotification notification ->
            ( state.notifications ++ [notification]
                |> asNotificationsIn state
            , Notify |> toCmdWithDelay 200
            )

        Notify ->
            ( state.notifications
                |> List.map
                    (\notification ->
                        { notification
                            | animation =
                                Animation.queue
                                    [ Animation.toWith
                                        ( Animation.easing
                                            { duration = 250
                                            , ease = \x -> x
                                            }
                                        )
                                        [ Animation.bottom (Animation.px 0)
                                        , Animation.opacity 1
                                        ]
                                    , Animation.wait (Time.millisToPosix 1500)
                                    , Animation.toWith
                                        ( Animation.easing
                                            { duration = 250
                                            , ease = \x -> x
                                            }
                                        )
                                        [ Animation.bottom (Animation.px -20)
                                        , Animation.opacity 0
                                        ]
                                    ]
                                    notification.animation
                        }
                    )
                |> asNotificationsIn state
            , RemoveNotification
                |> toCmdWithDelay 2000
            )

        RemoveNotification ->
            ( List.tail state.notifications
                |> Maybe.map (asNotificationsIn state)
                |> Maybe.withDefault state
            , Cmd.none
            )

        Animate subMsg ->
            ( state.notifications
                |> List.map
                    (\notification ->
                        { notification | animation = Animation.update subMsg notification.animation }
                    )
                |> asNotificationsIn state
            , Cmd.none
            )

        NoOp ->
            (state, Cmd.none)


asNotificationsIn : State -> List Notification -> State
asNotificationsIn state notifications =
    { state | notifications = notifications }


asTodosIn : State -> Todos -> State
asTodosIn state todos =
    { state | todos = todos }


clearNewTodo : State -> State
clearNewTodo state =
    { state | newTodoName = Nothing }


toCmd : msg -> Cmd msg
toCmd msg =
    Task.perform
        (always msg)
        (Task.succeed ())


toCmdWithDelay : Float -> msg -> Cmd msg
toCmdWithDelay time msg =
    Process.sleep time
        |> Task.andThen (always <| Task.succeed msg)
        |> Task.perform identity
