module View exposing (view)

import Html exposing (Html)
import Element exposing (..)
import Element.Font as Font
import Element.Input as Input
import Element.Background as Bg

import Msg exposing (Msg)
import Model.Todo exposing (Todo)
import State exposing (State)
import View.Heading
import View.Todos
import View.Stats
import View.NewTodo
import View.Notifications
import View.CommonStyles exposing (colors)


view : State -> Html Msg
view state =
    column
        [ centerX
        , centerY
        , spacing 25
        , padding 20
        , width <| maximum 800 fill
        , Font.size 14
        ]
        [ View.Heading.render
        , View.NewTodo.render state
        , View.Stats.render state
        , View.Todos.render state
        , View.Notifications.render state
        ]
        |> layout
            [ width fill
            , height fill
            , Bg.color <| .bg <| colors 1
            ]
